package ua.khpi.oop.kryzhanovskyi02;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
	public static List<Integer> divide(int test) {
		int temp = test;
		ArrayList<Integer> array = new ArrayList<Integer>();
		do {
			array.add(test % 10);
			temp /= 10;
		} while (test > 0);
		return array;
	}

	public static String toOctal(int decimal) {
		int rem;
		String octal = "";
		char octalchars[] = { '0', '1', '2', '3', '4', '5', '6', '7' };
		int count = 1;
		while (decimal > 0) {
			rem = decimal % 8;
			octal = octalchars[rem] + octal;
			decimal = decimal / 8;
			++count;
		}
		return octal;
	}

	public static void main(String args[]) {		 
		for(int i = 0; i<10; i++) {
		Random rand = new Random();
		int decimal = rand.nextInt(10000) & Integer.MAX_VALUE;
		List<Integer> array = divide(decimal);     
				
		
		System.out.println("Decimal of random octal is "+toOctal(decimal)+" "+decimal);
		}
	}
}
