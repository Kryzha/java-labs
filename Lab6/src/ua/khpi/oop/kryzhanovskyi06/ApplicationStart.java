package ua.khpi.oop.kryzhanovskyi06;

import ua.khpi.oop.kryzhanovskyi06.containers.InformationContainer;
import ua.khpi.oop.kryzhanovskyi06.util.Util;

import java.util.Scanner;

public class ApplicationStart {
    private static ApplicationStart instance = null;
    private InformationContainer container = new InformationContainer();

    private ApplicationStart() {}

    /**
     * Method that returns ApplicationStart instance
     * @return instance of current class
     */
    public static ApplicationStart getInstance() {
        if (instance == null) {
            instance = new ApplicationStart();
        }
        return instance;
    }

    /**
     * Method that starts application logic
     */
    public void start() {
        Scanner scanner = new Scanner(System.in);

            System.out.println("Please, type here your text ");
            String text = scanner.nextLine();
            System.out.println("Please, type here the lenght of words ");
            int number = inputIntNumber(scanner);
            container.add(Util.deleteWords(Util.cutString(text), number).toString());

        System.out.println("All words in this program: " + container + ". Size of the container is: " + container.size());
    }

    /**
     * Method that takes integer from input
     * @param scanner Scanner that will be used while taking a number from input
     * @return integer that was typed by user
     */
    private static int inputIntNumber(Scanner scanner) {
        int number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Wrong number typed, 0 was returned!");
        }
        return number;
    }

    public InformationContainer getContainer() {
        return container;
    }
}

