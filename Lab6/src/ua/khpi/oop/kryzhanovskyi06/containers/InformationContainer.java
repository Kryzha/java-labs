package ua.khpi.oop.kryzhanovskyi06.containers;
import java.io.Serializable;
import java.util.*;

public class InformationContainer extends AbstractCollection<String> implements Serializable {
    private static final int INITIAL_SIZE = 0;
    private String[] array = new String[INITIAL_SIZE];
    private int size = INITIAL_SIZE;

    public InformationContainer() {}

    /**
     * Method that returns current size of the container
     * @return current size of the container
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Method that checks if container contains passed element
     * @param s String that will be checked
     * @return true if container contains this element, false otherwise
     */
    public boolean contains(String s) {
        for (String element : array) {
            if (element.equals(s)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<String> iterator() {
        return new Iterator<>() {
            final String[] currentArray = InformationContainer.this.array;
            int currentElement = 0;

            /**
             * Method that checks if there is next element in the iteration
             * @return true if element is exists and false otherwise
             */
            @Override
            public boolean hasNext() {
                return currentArray.length > currentElement;
            }

            /**
             * Method that returns next element of iteration from the container
             * @return next element
             */
            @Override
            public String next() {
                return currentArray[currentElement++];
            }
        };
    }

    /**
     * Method that will be converting container to simple array
     * @return String array
     */
    @Override
    public Object[] toArray() {
        return array;
    }

    /**
     * Method that will add element to the container
     * @param s Element that will be added
     * @return true if element was added and false if something went wrong
     */
    @Override
    public boolean add(String s) {
        String[] newArray = Arrays.copyOf(array, ++size);
        newArray[newArray.length - 1] = s;
        array = newArray;
        return true;
    }

    /**
     * Method that removes first element that equals to passed one
     * @param o Element that will be removed
     * @return true if element was removed and false otherwise
     */
    @Override
    public boolean remove(Object o) {
        try {
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(o)) {
                    array[i] = null;
                    String[] newArray = new String[--size];
                    for (int count = 0, j = 0; j < array.length; j++) {
                        if (array[j] != null) {
                            newArray[count++] = array[j];
                        }
                    }
                    array = newArray;
                    return true;
                }
            }
            return false;
        } catch (ClassCastException e) {
            return false;
        }
    }

    /**
     * Method, that checks if passed container contains all elements in current container
     * @param container Container with which checking will be made
     * @return true if all elements are contains and false otherwise
     */
    public boolean containsAll(InformationContainer container) {
        String[] containerArray = (String[]) container.toArray();

        if (containerArray.length > array.length) return false;
        if (containerArray.length == 0 && array.length == 0) return false;
        for (int count = 0, i = 0; i < array.length; i++) {
            if (Objects.equals(array[i], containerArray[count])) {
                count++;
            } else {
                count = 0;
            }
            if (count == containerArray.length) return true;
        }
        return false;
    }

    /**
     * Method clears array (0 elements in it after clearing)
     */
    @Override
    public void clear() {
        size = INITIAL_SIZE;
        array = new String[size];
    }

    /**
     * Method to transfer container to String
     * @return String from array
     */
    @Override
    public String toString() {
        return Arrays.toString(array);
    }

    /**
     * Gets and String by index
     * @param index index of the element to be found
     * @return String that found or null otherwise
     */
    public String get(int index) {
        try {
            return array[index];
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Gets index of the first String that equals to passed
     * @param s String to find an index
     * @return index or null otherwise
     */
    public Integer getIndex(String s) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(s)) return i;
        }
        return null;
    }

    /**
     * Method to compare two containers
     * @param container container to compare with current
     * @return true if compare, false otherwise
     */
    public boolean compareContainer(InformationContainer container) {
        return Arrays.equals(container.toArray(), array);
    }

    /**
     * Sorting array by asc
     */
    public void sortByAsc() {
        Arrays.sort(array);
    }

    /**
     * Sorting array by desc
     */
    public void sortByDesc() {
        Arrays.sort(array, Collections.reverseOrder());
    }

    /**
     * Method that returns objects hashCode
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.size();
    }

    /**
     * Method that checks if the two objects are equals
     * @param obj Object to compare
     * @return true if equals, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        return obj.getClass().equals(this.getClass()) && this.compareContainer((InformationContainer) obj) && (this.size() == ((InformationContainer) obj).size());
    }
}
