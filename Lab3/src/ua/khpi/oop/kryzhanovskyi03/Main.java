package ua.khpi.oop.kryzhanovskyi03;

import java.util.Scanner;

public class Main {
		 
	public static void main(String[] args) {
		int number = 0;
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("Vvedite chislo:");
			number = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("Wrong number typed, 0 selected as a replace number!");
			number = 0;
		}

        String text = "zdes text iz kotorogo nuzhno udalit vse slova kotoryje nachinajutsja na soglasnuju bukvu";
        System.out.println(text);
        System.out.println("\n\n" + Util.deleteWords(Util.cutString(text), number).toString());
        
	}
}
