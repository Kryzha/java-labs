package ua.khpi.oop.kryzhanovskyi03;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

public class Util {
	private Util() {}
	  public static List<String> cutString(String text) {
	        List<String> stringList = new ArrayList<>();

	        Scanner scanner = new Scanner(text);
	        while (scanner.hasNext()) {
	            stringList.add(scanner.next());
	        }

	        return stringList;
	    }

	  
	  public static List<String> deleteWords(List<String> words, int length) {
		  words.removeIf(s -> {
			        return (!isVowel(s)) && s.length() == length;
			       
			});
		  return words;
	  }

	    public static boolean isVowel(String incomingText) {
	        switch ((char)Character.toLowerCase(incomingText.charAt(0))) {
	        case 'a':
	        case 'e':
	        case 'i':
	        case 'o':
	        case 'u':
	        case 'y':
	            return true;
	        default:
	            return false;
	 
	        }
	    }
}
