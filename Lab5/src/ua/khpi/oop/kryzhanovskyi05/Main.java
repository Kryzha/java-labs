package ua.khpi.oop.kryzhanovskyi05;

/**
 * 5 laboratory work
 * Make a container that will keep an elements from the task 4 (3 laboratory work)
 *
 * @version 1 10 Nov 2022
 * @author Kryzhanovskyi Illia
 */

import ua.khpi.oop.kryzhanovskyi05.Util.Util;
import ua.khpi.oop.kryzhanovskyi05.containers.InformationContainer;

import java.util.Scanner;


public class Main {
	/**
	 * The point of enter
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		InformationContainer container = new InformationContainer();

			System.out.println("Please, type here your text (in line): ");
			String text = scanner.nextLine();
			System.out.println("Please, type here lenght of word: ");
			int number = inputIntNumber(scanner);
			container.add(Util.deleteWords(Util.cutString(text), number).toString());
		
	
		System.out.println("All words in this program: " + container + ". Size of the container is: " + container.size());
	}
	/**
	 * Method that takes integer from input
	 * @param scanner Scenner that will be used while taking a number from input
	 * @return integer that was typed by user
	 */

		private static int inputIntNumber(Scanner scanner) {
			int number = 0;
			try {
				number = Integer.parseInt(scanner.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("Wrong number typed, 0 was returned!");
			}
			return number;
		}

}
