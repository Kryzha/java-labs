package ua.khpi.oop.kryzhanovskyi04;

import java.util.Scanner;

public class UiInterface {
	private boolean debug = false;
    private boolean settedValues = false;
    private String text;
    private int number;

    private void run(String text, int number) {
    	Util.deleteWords(Util.cutString(text), number).toString();
    }
	
    
    
    /**
     * method to set values for application running
     * */
    private void setValues() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, type here your text (in line): ");
        text = scanner.nextLine();
		try {
			System.out.println("Vvedite chislo:");
			number = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("Wrong number typed!");
			number = 0;
		}
        settedValues = true;
    }

    
    /**
     * method to display values of entered values
     * */
    private void displayValues() {
        System.out.println("Entered text: " + text);
        System.out.println("Entered size of words: " + number);
    }
    
	public void help() {

    }
	
	 private boolean matchOptions(String values) {
	        boolean result = false;
	        switch (values) {
	            case "1":
	                setValues();
	                break;
	            case "2":
	            	result = true;
	                if (settedValues) {
	                    displayValues();
	                } else {
	                    System.out.println("Values are not entered yet!");
	                }

	                break;
	            case "3":
	                if (settedValues) {
	                    result = true;
	                } else {
	                    System.out.println("The values are not selected!");
	                }
	                break;
	            case "4":
	                System.exit(0);
	                break;
	            default:
	                System.out.println("You printed wrong value!");
	        }
	        return result;
	    }

	
	 public void start() {
	        System.out.println();
	        System.out.println("Words deleting");
	        System.out.println("[Enter a number to choose an action]");
	        System.out.println("1 - Set values");
	        System.out.println("2 - Display values");
	        System.out.println("3 - Run program");
	        System.out.println("4 - Exit");
	        System.out.println();
	        String value = new Scanner(System.in).nextLine();
	        if (matchOptions(value)) {
	            run(text, number);
	        } else {
	            start();
	        }
	    }

	
	public void setDebug(boolean debug) {
        this.debug = debug;
    }
}