package ua.khpi.oop.kryzhanovskyi07;

import ua.khpi.oop.kryzhanovskyi07.models.AddressBook;
import ua.khpi.oop.kryzhanovskyi07.models.User;
import ua.khpi.oop.kryzhanovskyi07.models.classes.Address;
import ua.khpi.oop.kryzhanovskyi07.models.classes.PhoneNumber;

import java.util.Date;
import java.util.Scanner;

public class AddressBookProgram {
    private static AddressBookProgram instance = null;
    private User user;

    private AddressBook addressBook;

    private AddressBookProgram() {}

    public static AddressBookProgram getInstance() {
        if (instance == null) {
            instance = new AddressBookProgram();
        }

        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, register new account: ");
        user = register(scanner);
        System.out.println("Please, type number of addresses that you want to create: ");
        Address[] addresses = createListOfAddresses(inputIntNumber(scanner), scanner);
        System.out.println("------------------");
        System.out.println("Please, type number of phones that you want to create: ");
        PhoneNumber[] phoneNumbers = createListOfPhoneNumbers(inputIntNumber(scanner), scanner);
        addressBook = new AddressBook(user, phoneNumbers, addresses);
    }

    private static PhoneNumber[] createListOfPhoneNumbers(int length, Scanner scanner) {
        PhoneNumber[] phones = new PhoneNumber[length];
        String phone;
        for (int i = 0; i < phones.length; i++) {
            System.out.println("Please, type here a phone number number: ");
            phone = scanner.nextLine();
            try {
                phones[i] = PhoneNumber.createPhoneNumber(phone);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage() + " Try again!");
                phones[i] = null;
                --i;
            }
            System.out.println("------------------");
        }

        return phones;
    }

    private static User register(Scanner scanner) {
        String name, fatherName, secondName;
        int day, month, year;

        System.out.println("Please type your name: ");
        name = scanner.nextLine();
        System.out.println("Please type your second name: ");
        secondName = scanner.nextLine();
        System.out.println("Please type your father name: ");
        fatherName = scanner.nextLine();
        System.out.println("Please type a day of birth: ");
        day = inputIntNumber(scanner);
        System.out.println("Please type a month of birth: ");
        month = inputIntNumber(scanner);
        System.out.println("Please type a year of birth: ");
        year = inputIntNumber(scanner);
        Date birthDay = new Date(year - 1900, month - 1, day);

        return new User(name, secondName, fatherName, birthDay);
    }

    /**
     * Method that creates list of addresses in book from user input
     * @param length amount of address to be created
     * @param scanner scanner to asl user to input values
     * @return array of created address books
     */
    private static Address[] createListOfAddresses(int length, Scanner scanner) {
        Address[] addresses = new Address[length];
        String country, city, street;
        Integer apartmentNumber;
        for (int i = 0; i < addresses.length; i++) {
            System.out.println("Please, type a country: ");
            country = scanner.nextLine();
            System.out.println("Please, type a city: ");
            city = scanner.nextLine();
            System.out.println("Please, type a street: ");
            street = scanner.nextLine();
            System.out.println("Please, type a apartment number: ");
            apartmentNumber = inputIntNumber(scanner);
            addresses[i] = new Address(country, city, street, apartmentNumber);
            System.out.println("------------------");
        }
        return addresses;
    }

    /**
     * Method that takes integer from input
     * @param scanner Scenner that will be used while taking a number from input
     * @return integer that was typed by user
     */
    private static int inputIntNumber(Scanner scanner) {
        int number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Wrong number typed, 0 was returned!");
        }
        return number;
    }
}
