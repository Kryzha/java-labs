package ua.khpi.oop.kryzhanovskyi08.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ua.khpi.oop.kryzhanovskyi08.models.AddressBook;
import ua.khpi.oop.kryzhanovskyi08.models.User;
import ua.khpi.oop.kryzhanovskyi08.models.classes.Address;
import ua.khpi.oop.kryzhanovskyi08.models.classes.PhoneNumber;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ua.khpi.oop.kryzhanovskyi08.AddressBookProgram.XML_FILE_PATH;

public class XmlBooksHandler {
    private XmlBooksHandler() {}

    /**
     * Method that stores a container of address in xml file
     * @param addressBook Address book to be stored
     * @return true if the object was successfully stored, false otherwise
     */
    public static boolean storeAddresses(AddressBook addressBook) {
        boolean result = false;
        User user = addressBook.getUser();

        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element bookElement = document.createElement("addressBook");
            bookElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            Element userElement = document.createElement("user");

            Element nameElement = document.createElement("name");
            nameElement.setTextContent(user.getName());
            userElement.appendChild(nameElement);

            Element secondNameElement = document.createElement("secondName");
            secondNameElement.setTextContent(user.getSecondName());
            userElement.appendChild(secondNameElement);

            Element fatherNameElement = document.createElement("fatherName");
            fatherNameElement.setTextContent(user.getFatherName());
            userElement.appendChild(fatherNameElement);

            Element birthDayElement = document.createElement("birthDay");
            birthDayElement.setTextContent(user.getBirthDay().getDay() + "." + user.getBirthDay().getMonth() + "." + user.getBirthDay().getYear());
            userElement.appendChild(birthDayElement);

            bookElement.appendChild(userElement);

            Element updateDateTimeElement = document.createElement("updateDateTime");
            updateDateTimeElement.setTextContent(addressBook.getUpdateDateTime().toString());
            bookElement.appendChild(updateDateTimeElement);

            Element phoneNumbersElement = document.createElement("phoneNumbers");
            for (PhoneNumber number : addressBook.getPhoneNumbers()) {
                Element numberElement = document.createElement("phoneNumber");
                numberElement.setTextContent(number.getValue());
                phoneNumbersElement.appendChild(numberElement);
            }
            bookElement.appendChild(phoneNumbersElement);


            Element addressesElement = document.createElement("addresses");
            for (Address address : addressBook.getAddresses()) {
                Element addressElement = document.createElement("address");

                Element countryElement = document.createElement("country");
                countryElement.setTextContent(address.getCountry());
                addressElement.appendChild(countryElement);

                Element cityElement = document.createElement("city");
                cityElement.setTextContent(address.getCity());
                addressElement.appendChild(cityElement);

                Element streetElement = document.createElement("street");
                streetElement.setTextContent(address.getStreet());
                addressElement.appendChild(streetElement);

                Element apartmentElement = document.createElement("apartment");
                apartmentElement.setTextContent(address.getApartmentNumber().toString());
                addressElement.appendChild(apartmentElement);

                addressesElement.appendChild(addressElement);
            }
            bookElement.appendChild(addressesElement);

            document.appendChild(bookElement);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(XML_FILE_PATH));

            transformer.transform(domSource, streamResult);
            result = true;
        } catch (Exception e) {
            System.out.println("En error occurred while running the program: " + e.getMessage());
        }
        return result;
    }

    /**
     * Method that gets list of Addresses Books from xml file
     * @return list of AddressBook objects
     */
    public static AddressBook getAddresses() {
        AddressBook addressBook = null;
        User user;
        List<Address> addresses;
        List<PhoneNumber> phoneNumbers;

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);

        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(XML_FILE_PATH);

            Node bookNode = document.getElementsByTagName("addressBook").item(0);

            if (bookNode.getNodeType() == Node.ELEMENT_NODE) {
                Element bookElement = (Element) bookNode;
                String name = bookElement.getElementsByTagName("name").item(0).getTextContent();
                String fatherName = bookElement.getElementsByTagName("fatherName").item(0).getTextContent();
                String secondName = bookElement.getElementsByTagName("secondName").item(0).getTextContent();
                Date birthDay = prepareDateFromString(bookElement.getElementsByTagName("birthDay").item(0).getTextContent());
                user = new User(name, secondName, fatherName, birthDay);
                LocalDateTime updatedTime = LocalDateTime.now();

                phoneNumbers = new ArrayList<>();
                NodeList phonesNodeList = bookElement.getElementsByTagName("phoneNumber");
                for (int j = 0; j < phonesNodeList.getLength(); j++) {
                    try {
                        Node phoneNode = phonesNodeList.item(j);
                        if (phoneNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element phoneElement = (Element) phoneNode;
                            phoneNumbers.add(PhoneNumber.createPhoneNumber(phoneElement.getTextContent()));
                        }
                    } catch (NullPointerException ignore) {}
                }

                addresses = new ArrayList<>();
                NodeList addressesNodeList = bookElement.getElementsByTagName("address");
                for (int k = 0; k < addressesNodeList.getLength(); k++) {
                    try {
                        Node addressNode = addressesNodeList.item(k);
                        if (addressNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element addressElement = (Element) addressNode;

                            String country = addressElement.getElementsByTagName("country").item(0).getTextContent();
                            String city = addressElement.getElementsByTagName("city").item(0).getTextContent();
                            String street = addressElement.getElementsByTagName("street").item(0).getTextContent();
                            int apartmentNumber = 0;
                            try {
                                apartmentNumber = Integer.parseInt(addressElement.getElementsByTagName("apartment").item(0).getTextContent());
                            } catch (NumberFormatException ignore) {}

                            addresses.add(new Address(country, city, street, apartmentNumber));
                        }
                    } catch (NullPointerException ignore) {}
                }
                addressBook = new AddressBook(user, addresses, phoneNumbers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addressBook;
    }

    /**
     * Method that converts string to date
     */
    private static Date prepareDateFromString(String dateString) {
        String[] dateStringArray = dateString.split("\\.");
        return new Date((Integer.parseInt(dateStringArray[2]) - 1900), (Integer.parseInt(dateStringArray[1]) - 1), Integer.parseInt(dateStringArray[0]));
    }
}
