package ua.khpi.oop.kryzhanovskyi08;

import ua.khpi.oop.kryzhanovskyi08.models.AddressBook;
import ua.khpi.oop.kryzhanovskyi08.utils.AddessBookInput;
import java.util.Scanner;
import static ua.khpi.oop.kryzhanovskyi08.utils.ApplicationCLIInput.getAccept;
import static ua.khpi.oop.kryzhanovskyi08.utils.XmlBooksHandler.getAddresses;
import static ua.khpi.oop.kryzhanovskyi08.utils.XmlBooksHandler.storeAddresses;

public class AddressBookProgram {
    public static final String XML_FILE_PATH = "./addressbook.xml";
    private static AddressBookProgram instance = null;

    private AddressBook addressBook;

    private AddressBookProgram() {}

    public static AddressBookProgram getInstance() {
        if (instance == null) {
            instance = new AddressBookProgram();
        }

        return instance;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        if (getAccept(scanner, "Please, type y if you want to create address books or n if you want to read")) {
            addressBook = AddessBookInput.inputAddressBook();
            if (storeAddresses(addressBook)) {
                System.out.println("AddressBook was successfully stored in .xml file");
            } else {
                System.out.println("Unfortunately we cannot store your AddressBook in .xml file!");
            }
        } else {
            System.out.println("Reading objects from .xml file: ");
            addressBook =  getAddresses();
        }
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }
}
