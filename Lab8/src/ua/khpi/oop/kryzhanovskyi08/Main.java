package ua.khpi.oop.kryzhanovskyi08;

/**
 * 8 laboratory work
 * Save and read a container from object that was created in lab07 without serialization, make dialog with user
 *
 * @version 1 05 Dec 2022
 * @author Illia Kryzhanovskyi
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		AddressBookProgram program = AddressBookProgram.getInstance();
		program.start();
		System.out.println(program.getAddressBook());
	}
}
