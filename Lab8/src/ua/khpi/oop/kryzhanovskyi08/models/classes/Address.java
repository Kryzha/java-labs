package ua.khpi.oop.kryzhanovskyi08.models.classes;

public class Address {
    String city;
    String country;
    String street;
    Integer apartmentNumber;

    public Address(String country, String city, String street, Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
        this.city = city;
        this.country = country;
        this.street = street;
    }

    public Address(String country, String city, String street) {
        this(country, city, street, null);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Integer apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    @Override
    public String toString() {
        return "[" + country + ", " + city + ", " + street + ", " + apartmentNumber + "]";
    }
}
