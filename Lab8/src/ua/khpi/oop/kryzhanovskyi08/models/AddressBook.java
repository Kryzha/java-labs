package ua.khpi.oop.kryzhanovskyi08.models;

import ua.khpi.oop.kryzhanovskyi08.models.classes.Address;
import ua.khpi.oop.kryzhanovskyi08.models.classes.PhoneNumber;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddressBook {
    private User user;
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();
    private List<Address> addresses = new ArrayList<>();
    private LocalDateTime updateDateTime = LocalDateTime.now();

    public AddressBook(User user, List<Address> addresses, List<PhoneNumber> phones) {
        this.addresses = addresses;
        this.user = user;
        this.phoneNumbers = phones;
    }

    public AddressBook(User user, Address... addresses) {
        this.user = user;
        this.addresses = List.of(addresses);
    }

    public AddressBook(User user, PhoneNumber... phones) {
        this.user = user;
        this.phoneNumbers = List.of(phones);
    }

    public AddressBook(User user, PhoneNumber[] phoneNumbers, Address[] addresses) {
        this.user = user;
        this.phoneNumbers = List.of(phoneNumbers);
        this.addresses = List.of(addresses);
    }

    public AddressBook(User user) {
        this.user = user;
    }

    public PhoneNumber getPhone(int index) {
        return phoneNumbers.get(index);
    }

    public Address getAddress(int index) {
        return addresses.get(index);
    }

    public void setPhoneNumber(int index, PhoneNumber value) {
        phoneNumbers.set(index, value);
    }

    public void setAddress(int index, Address address) {
        addresses.set(index, address);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "[ "
                + user
                + Arrays.toString(phoneNumbers.toArray()) + " "
                + Arrays.toString(addresses.toArray()) + " "
                + "updated time: " + updateDateTime + "]";
    }
}
